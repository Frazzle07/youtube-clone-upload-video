import { S3Event } from 'aws-lambda';
import * as AWS from 'aws-sdk';
import {
  CreateJobCommand,
  CreateJobCommandOutput,
  MediaConvertClient,
} from '@aws-sdk/client-mediaconvert';

// Set the AWS Region.
const REGION = 'us-west-2';
//Set the account end point.
const ENDPOINT = 'https://hvtjrir1c.mediaconvert.us-west-2.amazonaws.com';
//Set the MediaConvert Service Object
const emcClient = new MediaConvertClient({
  region: REGION,
  endpoint: ENDPOINT,
});

// Set the Region
AWS.config.update({ region: REGION });
// Set the custom endpoint for your account
// snippet-end:[mediaconvert.JavaScript.jobs.createJob_config]

export const invoke = async (
  event: S3Event,
): Promise<CreateJobCommandOutput | undefined> => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  console.log(event.Records);
  const bucket = event.Records[0].s3.bucket.name;
  const key = decodeURIComponent(
    event.Records[0].s3.object.key.replace(/\+/g, ' '),
  );

  console.log(`Bucket: ${bucket}`);
  console.log(`Bucket: ${key}`);

  const params = {
    Queue: 'arn:aws:mediaconvert:us-west-2:618177279545:queues/Default',
    Role: 'arn:aws:iam::618177279545:role/service-role/MediaConvert_Default_Role',
    Settings: {
      TimecodeConfig: {
        Source: 'ZEROBASED',
      },
      OutputGroups: [
        {
          Name: 'DASH ISO',
          Outputs: [
            {
              ContainerSettings: {
                Container: 'MPD',
              },
              VideoDescription: {
                CodecSettings: {
                  Codec: 'H_264',
                  H264Settings: {
                    MaxBitrate: 500000,
                    RateControlMode: 'QVBR',
                    SceneChangeDetect: 'TRANSITION_DETECTION',
                  },
                },
              },
              NameModifier: 'Video Ouput',
            },
            {
              ContainerSettings: {
                Container: 'MPD',
              },
              AudioDescriptions: [
                {
                  AudioSourceName: 'Audio Selector 1',
                  CodecSettings: {
                    Codec: 'AAC',
                    AacSettings: {
                      Bitrate: 96000,
                      CodingMode: 'CODING_MODE_2_0',
                      SampleRate: 48000,
                    },
                  },
                },
              ],
              NameModifier: 'Audio Output',
            },
          ],
          OutputGroupSettings: {
            Type: 'DASH_ISO_GROUP_SETTINGS',
            DashIsoGroupSettings: {
              SegmentLength: 5,
              Destination: 's3://transcoded-videos-abc123/',
              FragmentLength: 2,
            },
          },
        },
      ],
      Inputs: [
        {
          AudioSelectors: {
            'Audio Selector 1': {
              DefaultSelection: 'DEFAULT',
            },
          },
          VideoSelector: {},
          TimecodeSource: 'ZEROBASED',
          FileInput: `s3://${bucket}/${key}`,
        },
      ],
    },
    AccelerationSettings: {
      Mode: 'DISABLED',
    },
    StatusUpdateInterval: 'SECONDS_60',
    Priority: 0,
  };

  try {
    const data = await emcClient.send(new CreateJobCommand(params));
    console.log('Job created!', data);
    return data;
  } catch (err) {
    console.log('Error', err);
  }
};
