import { APIGatewayProxyResult } from 'aws-lambda';
import * as AWS from 'aws-sdk';

const s3 = new AWS.S3({ signatureVersion: 'v4' });
const URL_EXPIRATION_SECONDS = 3000;

export const invoke = async (): Promise<APIGatewayProxyResult> => {
  return await getUploadURL();
};

const getUploadURL = async function () {
  const randomID = Math.random() * 10000000;
  const Key = `${randomID}.mp4`;

  // Get signed URL from S3
  const s3Params = {
    Bucket: 'video-uploads-abc123',
    Key,
    Expires: URL_EXPIRATION_SECONDS,
    ContentType: 'video/mp4',
  };

  console.log('Params: ', s3Params);
  const uploadURL = await s3.getSignedUrlPromise('putObject', s3Params);

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    body: JSON.stringify({
      uploadURL: uploadURL,
      Key,
    }),
  };
};
