// Import path for resolving file paths
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  // Specify the entry point for our app.
  entry: {
    mediaConvert: './src/mediaConvert/index.ts',
    preSignedUrl: './src/preSignedUrl/index.ts',
  },
  mode: 'production',
  cache: true,
  target: 'node',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  // Specify the output file containing our bundled code
  output: {
    libraryTarget: 'commonjs',
    path: path.resolve(__dirname, './dist/'),
    filename: '[name]/index.js',
    clean: true,
  },
};
