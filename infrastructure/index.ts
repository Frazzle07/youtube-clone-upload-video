import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as apigateway from '@aws-cdk/aws-apigateway';
import * as s3 from '@aws-cdk/aws-s3';
import * as iam from '@aws-cdk/aws-iam';
import * as lambdaEventSources from '@aws-cdk/aws-lambda-event-sources';
import * as cloudfront from '@aws-cdk/aws-cloudfront';
import * as origins from '@aws-cdk/aws-cloudfront-origins';

export class UploadVideoStack extends cdk.Stack {
  constructor(app: cdk.App, id: string) {
    super(app, id);

    const videoUploadLambda = new lambda.Function(this, 'PreSignedUrlLambda', {
      functionName: 'PreSignedUrlLambda',
      code: lambda.Code.fromAsset('dist/preSignedUrl'),
      handler: 'index.invoke',
      timeout: cdk.Duration.seconds(300),
      runtime: lambda.Runtime.NODEJS_14_X,
    });

    const api = new apigateway.RestApi(this, 'VideoUpload');

    const videoUploadIntegration = new apigateway.LambdaIntegration(
      videoUploadLambda,
    );

    const mediaConvertLambda = new lambda.Function(this, 'MediaConvertLambda', {
      functionName: 'MediaConvertLambda',
      code: lambda.Code.fromAsset('dist/mediaConvert'),
      handler: 'index.invoke',
      timeout: cdk.Duration.seconds(300),
      runtime: lambda.Runtime.NODEJS_14_X,
    });

    const mediaConvertLambdaPolicy = new iam.PolicyStatement({
      actions: ['iam:PassRole', 'mediaconvert:*'],
      resources: ['*'],
    });

    mediaConvertLambda.role?.attachInlinePolicy(
      new iam.Policy(this, 'media-convert-lambda-pass-role-policy', {
        statements: [mediaConvertLambdaPolicy],
      }),
    );

    api.root.addResource('upload').addMethod('GET', videoUploadIntegration);

    const uploadVideoBucket = new s3.Bucket(this, 'UploadVideoBucket', {
      bucketName: 'video-uploads-abc123',
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      cors: [
        {
          allowedHeaders: ['*'],
          allowedOrigins: ['*'],
          allowedMethods: [
            s3.HttpMethods.GET,
            s3.HttpMethods.PUT,
            s3.HttpMethods.HEAD,
          ],
          maxAge: 3000,
        },
      ],
    });

    const transcodedVideoBucket = new s3.Bucket(this, 'TranscodedVideoBucket', {
      bucketName: 'transcoded-videos-abc123',
      cors: [
        {
          allowedHeaders: ['*'],
          allowedOrigins: ['*'],
          allowedMethods: [
            s3.HttpMethods.GET,
            s3.HttpMethods.PUT,
            s3.HttpMethods.HEAD,
          ],
          maxAge: 3000,
        },
      ],
    });

    // const myBucket = new s3.Bucket(this, 'myBucket');
    // new cloudfront.Distribution(this, 'myDist', {
    //   defaultBehavior: { origin: new origins.S3Origin(myBucket) },
    // });

    // new cloudfront.Distribution(this, 'VideoDistribution', {
    //   defaultBehavior: { origin: new origins.S3Origin(transcodedVideoBucket) },
    // });

    const s3PutEventSource = new lambdaEventSources.S3EventSource(
      uploadVideoBucket,
      {
        events: [s3.EventType.OBJECT_CREATED_PUT],
      },
    );

    mediaConvertLambda.addEventSource(s3PutEventSource);

    const role = new iam.Role(this, 'MediaConvertRole', {
      assumedBy: new iam.ServicePrincipal('mediaconvert.amazonaws.com'),
    });

    role.addToPolicy(
      new iam.PolicyStatement({
        resources: ['*'],
        actions: ['s3:*'],
        effect: iam.Effect.ALLOW,
      }),
    );

    role.addToPolicy(
      new iam.PolicyStatement({
        resources: ['arn:aws:execute-api:*:*:*'],
        actions: ['execute-api:Invoke', 'execute-api:ManageConnections'],
        effect: iam.Effect.ALLOW,
      }),
    );
  }
}

const app = new cdk.App();
new UploadVideoStack(app, 'UploadVideoStack');
